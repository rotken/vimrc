#!/bin/sh

# curl 
curl -k https://northstar.tamucc.edu/mlongoria1/vimrc/raw/master/.vimrc > .vimrc
# clone
git clone https://github.com/powerline/fonts.git --depth=1
# install
cd fonts
./install.sh
# clean-up a bit
cd ..
rm -rf fonts
